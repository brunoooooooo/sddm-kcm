msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-14 00:39+0000\n"
"PO-Revision-Date: 2024-03-09 04:45\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/sddm-kcm/kcm_sddm.pot\n"
"X-Crowdin-File-ID: 43053\n"

#: sddmauthhelper.cpp:385 sddmauthhelper.cpp:407 sddmauthhelper.cpp:414
msgid "Invalid theme package"
msgstr "无效的主题包"

#: sddmauthhelper.cpp:391
msgid "Could not open file"
msgstr "无法打开文件"

#: sddmauthhelper.cpp:422
msgid "Could not decompress archive"
msgstr "无法解压主题包"

#: sddmthemeinstaller.cpp:29
#, kde-format
msgid "SDDM theme installer"
msgstr "SDDM 主题安装程序"

#: sddmthemeinstaller.cpp:36
#, kde-format
msgid "Install a theme."
msgstr "安装主题。"

#: sddmthemeinstaller.cpp:37
#, kde-format
msgid "Uninstall a theme."
msgstr "卸载主题。"

#: sddmthemeinstaller.cpp:39
#, kde-format
msgid "The theme to install, must be an existing archive file."
msgstr "要安装的主题包，必须是一个保存在本机中的压缩文件。"

#: sddmthemeinstaller.cpp:65
#, kde-format
msgid "Unable to install theme"
msgstr "无法安装主题"

#: src/sddmkcm.cpp:155 src/sddmkcm.cpp:291
msgid ""
"Cannot proceed, user 'sddm' does not exist. Please check your SDDM install."
msgstr "无法继续，用户“sddm”不存在。请检查本系统的 SDDM 安装状态是否正确。"

#: src/sessionmodel.cpp:84
#, kde-format
msgctxt "%1 is the name of a session"
msgid "%1 (Wayland)"
msgstr "%1 (Wayland)"

#: src/ui/Advanced.qml:17
#, kde-format
msgctxt "@title"
msgid "Behavior"
msgstr "行为设置"

#: src/ui/Advanced.qml:23
#, kde-format
msgctxt "option:check"
msgid "Automatically log in:"
msgstr "自动登录："

#: src/ui/Advanced.qml:28
#, kde-format
msgctxt ""
"@label:listbox, the following combobox selects the user to log in "
"automatically"
msgid "as user:"
msgstr "作为用户："

#: src/ui/Advanced.qml:92
#, kde-format
msgctxt ""
"@label:listbox, the following combobox selects the session that is started "
"automatically"
msgid "with session"
msgstr "使用会话"

#: src/ui/Advanced.qml:124
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Auto-login does not support unlocking your KDE Wallet automatically, so it "
"will ask you to unlock it every time you log in.<nl/><nl/>To avoid this, you "
"can change the wallet to have a blank password. Note that this is insecure "
"and should only be done in a trusted environment."
msgstr ""
"自动登录不支持自动解锁 KDE 密码库，您必须在每次登录之后手动解锁它。<nl/><nl/>"
"要想避免这种情况，可将密码库的密码设为空白。请注意：空白密码是不安全的，请只"
"在可以信任的环境中如此设置。"

#: src/ui/Advanced.qml:129
#, kde-format
msgid "Open KDE Wallet Settings"
msgstr "打开 KDE 密码库设置"

#: src/ui/Advanced.qml:135
#, kde-format
msgctxt "@option:check"
msgid "Log in again immediately after logging off"
msgstr "注销后立即重新登录"

#: src/ui/Advanced.qml:148
#, kde-format
msgctxt "@label:spinbox"
msgid "Minimum user UID:"
msgstr "最小用户 UID："

#: src/ui/Advanced.qml:160
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum user UID:"
msgstr "最大用户 UID："

#: src/ui/Advanced.qml:175
#, kde-format
msgctxt "@label:textbox"
msgid "Halt Command:"
msgstr "关机命令："

#: src/ui/Advanced.qml:208
#, kde-format
msgctxt "@label:textbox"
msgid "Reboot Command:"
msgstr "重启命令："

#: src/ui/DetailsDialog.qml:26
#, kde-format
msgctxt "@title:window, %1 is the theme name, %2 the version"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: src/ui/DetailsDialog.qml:57
#, kde-format
msgid "No preview available"
msgstr "无可用预览"

#: src/ui/DetailsDialog.qml:60
#, kde-format
msgctxt ""
"%1 is a description of the theme, %2 are the authors, %3 is the license"
msgid "%1, by %2 (%3)"
msgstr "%1，由 %2 设计制作，(%3 许可证)"

#: src/ui/main.qml:24
#, kde-format
msgctxt "@action:button"
msgid "Behavior…"
msgstr "行为…"

#: src/ui/main.qml:29
#, kde-format
msgctxt "@action:button"
msgid "Apply Plasma Settings…"
msgstr "应用 Plasma 设置…"

#: src/ui/main.qml:34
#, kde-format
msgctxt "@action:button"
msgid "Install From File…"
msgstr "安装文件…"

#: src/ui/main.qml:39
#, kde-format
msgctxt "@action:button as in, \"get new SDDM themes\""
msgid "Get New…"
msgstr "获取新主题…"

#: src/ui/main.qml:89
#, kde-format
msgctxt "@info:tooltip"
msgid "View details"
msgstr "查看详细信息"

#: src/ui/main.qml:104
#, kde-format
msgctxt "@info:tooltip"
msgid "Change Background"
msgstr "更改背景"

#: src/ui/main.qml:113
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete"
msgstr "删除"

#: src/ui/main.qml:133
#, kde-format
msgctxt "@title:window"
msgid "Apply Plasma Settings"
msgstr "应用 Plasma 设置"

#: src/ui/main.qml:139
#, kde-format
msgid ""
"This will make the SDDM login screen reflect your customizations to the "
"following Plasma settings:"
msgstr "这将使 SDDM 登录屏幕使用下列用户定义的 Plasma 设置："

#: src/ui/main.qml:140
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para><list><item>Color scheme,</item><item>Cursor theme,</item><item>Cursor "
"size,</item><item>Font,</item><item>Font rendering,</item><item>NumLock "
"preference,</item><item>Plasma theme,</item><item>Scaling DPI,</"
"item><item>Screen configuration (Wayland only)</item></list></para>"
msgstr ""
"<para><list><item>配色方案、</item><item>光标主题、</item><item>光标大小、</"
"item><item>字体、</item><item>字体渲染方式、</item><item>数字键锁定首选项、</"
"item><item>Plasma 主题、</item><item>缩放 DPI、</item><item>屏幕配置 (仅适用"
"于 Wayland)</item></list></para>"

#: src/ui/main.qml:141
#, kde-format
msgid ""
"Please note that theme files must be installed globally to be reflected on "
"the SDDM login screen."
msgstr "提示：主题文件必须安装到系统全局才能被 SDDM 登录屏幕使用。"

#: src/ui/main.qml:150
#, kde-format
msgctxt "@action:button"
msgid "Apply"
msgstr "应用"

#: src/ui/main.qml:155
#, kde-format
msgctxt "@action:button"
msgid "Reset to Default Settings"
msgstr "重置为默认设置"

#: src/ui/main.qml:165
#, kde-format
msgctxt "@title:window"
msgid "Change Background"
msgstr "更改背景"

#: src/ui/main.qml:174
#, kde-format
msgid "No image selected"
msgstr "未选择图像"

#: src/ui/main.qml:202
#, kde-format
msgctxt "@action:button"
msgid "Load From File…"
msgstr "加载文件…"

#: src/ui/main.qml:207
#, kde-format
msgctxt "@action:button"
msgid "Clear Image"
msgstr "清除图片"
